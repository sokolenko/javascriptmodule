import Main.Companion.readFileAsTextUsingInputStream
import org.mozilla.javascript.Context
import org.mozilla.javascript.Function
import java.io.File

fun main(args: Array<String>) {
    val main = Main()
    //main.start()
    //main.testJs()
    main.parse(readFileAsTextUsingInputStream("body.txt"))
}


class Main {
    fun start() {
        val str = readFileAsTextUsingInputStream("body.txt")
        println(str)
    }

    fun testJs() {
        val script = ("function abc(x,y) {" +
                "var txt = '{\"name\":\"John\", \"age\":30, \"city\":\"New York\"}'\n" +
                "var obj = JSON.parse(txt);\n" +
                "return x+y;\n" +
                "}\n"
                + "function def(u,v) {return u-v;}")
        val context = Context.enter()
        try {
            val scope = context.initStandardObjects()
            context.evaluateString(scope, script, "script", 1, null)
            val fct = scope["abc", scope] as Function
            val result = fct.call(
                context, scope, scope, arrayOf<Any>(87, 3)
            )
            println(Context.jsToJava(result, Int::class.javaPrimitiveType))
        } catch (t: Throwable) {
            t.printStackTrace()
        } finally {
            Context.exit()
        }
    }

    fun parse(body: String) {
        val context = Context.enter()
        context.optimizationLevel = -1
        try {
            val scope = context.initStandardObjects()
            context.evaluateString(scope, parser, "script", 1, null)
            val fct = scope["formatsArr", scope] as Function
            val result = fct.call(
                context, scope, scope, arrayOf<Any>(body)
            )
            //println(Context.jsToJava(result, String::class.javaPrimitiveType))
            println(result)
        } catch (t: Throwable) {
            t.printStackTrace()
        } finally {
            Context.exit()
        }
    }



    companion object {

        fun readFileAsTextUsingInputStream(fileName: String)
                = File(fileName).readText()

        private const val parser = "function formatsArr(str) {\n" +
                "                var re = /var ytInitialPlayerResponse\\s*=\\s*(\\{.+?\\})\\s*\\<\\/script\\>/;\n" +
                "                var found = str.match(re);\n" +
                "return found[1];}"

        private val parser2 = "function formatsArr(str) {\n" +
                "    var re = /var ytInitialPlayerResponse\\s*=\\s*(\\{.+?\\})\\s*\\;/;\n" +
                "    var found = str.match(re);\n" +
                "    var ytInitialPlayerResponse = JSON.parse(found[1].replace(/\\<\\/.*/g, ''))\n" +
                "    if (typeof ytInitialPlayerResponse == 'object') {\n" +
                "        var formats = (Array.isArray(ytInitialPlayerResponse?.streamingData?.formats)) ? ytInitialPlayerResponse?.streamingData?.formats : [];\n" +
                "        var adaptiveFormats = (Array.isArray(ytInitialPlayerResponse?.streamingData?.adaptiveFormats)) ? ytInitialPlayerResponse?.streamingData?.adaptiveFormats : [];\n" +
                "        var result = formats.concat(adaptiveFormats);\n" +
                "        return result;\n" +
                "    }\n" +
                "}"
    }
}